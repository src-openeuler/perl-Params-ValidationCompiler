%bcond_with perl_Params_ValidationCompiler_enables_optional_test

Name:          perl-Params-ValidationCompiler
Version:       0.31
Release:       2
Summary:       Build an optimized subroutine parameter validator once, use it forever
License:       Artistic-2.0
URL:           https://metacpan.org/release/Params-ValidationCompiler
Source0:       https://cpan.metacpan.org/modules/by-module/Params/Params-ValidationCompiler-%{version}.tar.gz
BuildArch:     noarch
# Build
BuildRequires: coreutils
BuildRequires: make
BuildRequires: perl-generators
BuildRequires: perl-interpreter
BuildRequires: perl(ExtUtils::MakeMaker) > 6.75
# Module
BuildRequires: perl(B)
BuildRequires: perl(Carp)
BuildRequires: perl(Class::XSAccessor)
BuildRequires: perl(Eval::Closure)
BuildRequires: perl(Exception::Class)
BuildRequires: perl(Exporter)
BuildRequires: perl(List::Util) >= 1.29
BuildRequires: perl(overload)
BuildRequires: perl(Scalar::Util)
BuildRequires: perl(strict)
BuildRequires: perl(warnings)
# Optional Functionality
BuildRequires: perl(Sub::Util) >= 1.40
# Test Suite
BuildRequires: perl(File::Spec)
BuildRequires: perl(Specio) >= 0.14
BuildRequires: perl(Specio::Declare)
BuildRequires: perl(Specio::Library::Builtins)
BuildRequires: perl(Test2::Plugin::NoWarnings)
BuildRequires: perl(Test2::Require::Module)
BuildRequires: perl(Test2::V0)
BuildRequires: perl(Test::More) >= 1.302015
BuildRequires: perl(Test::Without::Module)
%if %{with perl_Params_ValidationCompiler_enables_optional_test}
# Optional Tests
BuildRequires: perl(Const::Fast)
BuildRequires: perl(CPAN::Meta) >= 2.120900
BuildRequires: perl(CPAN::Meta::Prereqs)
BuildRequires: perl(Hash::Util)
%if !%{defined perl_bootstrap}
BuildRequires: perl(Moose::Util::TypeConstraints)
BuildRequires: perl(Types::Standard)
%endif
%endif
# Dependencies
Recommends: perl(Class::XSAccessor)
Recommends: perl(Sub::Util) >= 1.40

%description
Create a customized, optimized, non-lobotomized, uncompromised, and thoroughly
specialized parameter checking subroutine.

%prep
%setup -q -n Params-ValidationCompiler-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
make install DESTDIR=%{buildroot}
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes CODE_OF_CONDUCT.md CONTRIBUTING.md eg/ README.md
%{perl_vendorlib}/Params/
%{_mandir}/man3/Params::ValidationCompiler.3*
%{_mandir}/man3/Params::ValidationCompiler::Compiler.3*
%{_mandir}/man3/Params::ValidationCompiler::Exceptions.3*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 0.31-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Jul 15 2024 gss <guoshengsheng@kylinos.cn> - 0.31-1
- update to version 0.31
- Require Class::XSAccessor 1.17+ when trying to load it. Earlier versions cause test failures. Reported by David Cantrell. GH #27.

* Wed Jun 29 2022 misaka00251 <misaka00251@misakanet.cn> - 0.30-1
- Init package (Thanks to fedora team)
